<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserTest extends TestCase
{

    // This trait automatically handles rollback the database after each test and migrate it before the next test.
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserTable()
    {
        $user = factory(App\User::class)->create([
            'name' => 'Abigail',
        ]);
        $this->seeInDatabase('users', [
            'name' => 'Abigail'
        ]);
    }

    public function testCreateUserShouldWorkCorrectly()
    {
        $this->visit('/user/create')
            ->type('Taylor', 'name')
            ->type("a@b.com", 'email')
            ->press('Opslaan')
            ->seeInDatabase('users', [
                'name' => 'Taylor'
            ])
            ->seePageIs('/user')
            ->see('Success');
    }

    public function testEditUserShouldWorkCorrectly() {
        $name1 = 'David';
        $name2 = 'Matt';
        $user = factory(App\User::class)->create([
            'id' => 1,
            'name' => $name1,
        ]);
        $this->visit('/user')
            ->see($name1)
            ->visit('/user/1/edit')
            ->see($name1)
            ->type($name2, 'name')
            ->press('Opslaan')
            ->seeInDatabase('users', [
                'name' => $name2
            ])
            ->seePageIs('/user')
            ->see('Success');
    }
}
